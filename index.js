const { cloneDeep } = require('lodash')

const _spies = {
  redirect: [],
  error: [],
  $axios: {
    get: undefined,
    post: undefined,
    put: undefined,
    patch: undefined,
    delete: undefined
  },
  $router: []
}

function buildAxiosDefaultObject (_this) {
  return ['get', 'post', 'put', 'patch', 'delete']
    .reduce(function (o, k) {
      o[k] = function () {
        const args = arguments
        return new Promise(function (resolve, reject) {
          _this.setAxiosSpy(new AxiosSpy(k, args))
          resolve({ data: {} })
        })
      }
      return o
    }, {})
}

function buildNuxtDefaultObject (_this) {
  return {
    error (e) { _this.spies.error.push(e) }
  }
}

function shallowBindScopeRecursively (_this, object, initialValue, recur = true) {
  return Array.isArray(object)
    ? object
    : Object.keys(object || {})
      .reduce((o, k) => {
        switch (typeof object[k]) {
          case 'function': o[k] = object[k].bind(_this); break
          case 'object':
            // avoids recursive nightmares with stuff like the vuex store.
            if (recur) o[k] = shallowBindScopeRecursively(_this, object[k], initialValue[k], false)
            else o[k] = object[k]
            break
          default: o[k] = object[k]
        }
        return o
      }, initialValue || {})
}

/**
 * A standardised spy class to represent Axios observers
 * @param {String} requestType - the request verb
 * @param {Object} args - the Array-like argument object of the caller: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments  
 */
class AxiosSpy {
  constructor (requestType, args = []) {
    this.path = args[0]
    this.body = args[1]
    this.requestType = requestType.toLowerCase()
  }
}

/**
 * A class that helps to support Nuxt SSR within a Jest context
 * @param {Object} context - an object containing overrides of any given context property.  
 ** functions supplied to the overrides object will automatically have the mockNuxt instance scope injected
 ** into them via .bind() allowing the client to access methods such as .setAxiosSpy etc. within the call
 ** functions within objects such as .$axios will also have the scope bound to them (to a depth of 1 level
 ** any deeper could interfere with existing objects such as a vuex store)
 */
class MockNuxt {
  constructor (context) {
    const _this = this

    this.spies = cloneDeep(_spies)

    this.context = shallowBindScopeRecursively(
      _this,
      context,
      {
        store: {},
        $axios: buildAxiosDefaultObject(_this),
        error (e) { _this.spies.error.push(e) },
        params: {},
        redirect (r) { _this.spies.redirect.push(r) }
      })
  }

  // GETTERS
  get errorSpy () { return this.spies.error }
  get redirectSpy () { return this.spies.redirect }
  get routerSpy () { return this.spies.$router }

  // METHODS
  /**
   * call the server side functions on the supplied component and return
   * @param {Component} component - the Vue component to perform SSR on
   * @param {Object} dataOverrides - data to append/override from the returndata object
   */
  async callServerSideMethods (component, dataOverrides) {
    if (component.middleware) await component.middleware(this.context)
    if (component.fetch) await component.fetch(this.context)
    if (component.asyncData) {
      const asyncData = await component.asyncData(this.context)
      component.data = function () { return { ...asyncData, ...dataOverrides } }
    }
    return component
  }
  /**
   * empty the mockNuxt instances `spies` object. Replace it with a brand clone.
   */
  clearSpies () {
    this.spies = cloneDeep(_spies)
  }
  /**
   * get the relevant axios spy from the mockNuxt instance's `spies` object
   * @param {String} type - the request verb type
   */
  getAxiosSpyByType (type) {
    return this.spies.$axios[type]
  }
  /**
   * globally available variables such as this.$axios to be injected into vue-test-utils `mock` property
   * @param {Object} plugins - override or mixin additional plugin stubs
   ** plugins.$axios methods will automatically be injected into the mockNuxt instance context
   ** allowing the client to access methods such as .setAxios etc. within the call
   */
  mockPlugins (plugins) {
    const _this = this
    const _plugins = plugins || {}
    return shallowBindScopeRecursively(
      _this,
      _plugins,
      {
        $axios: buildAxiosDefaultObject(
          _this, _plugins.hasOwnProperty('$axios') ? _plugins.$axios : {}),
        $nuxt: buildNuxtDefaultObject(_this),
        $router: { push (v) { _this.spies.$router.push(v) } }
      })
  }
  /**
   * method to set an axios spy within the mockNuxt's `spies` object
   * @param {String/AxiosSpy} obj - either the request name or AxiosSpy instance
   * @param {Object} value - optional custom object if first object was a string
   */
  setAxiosSpy (obj, value) {
    if (obj instanceof AxiosSpy) this.spies.$axios[obj.requestType] = obj
    else this.spies.$axios[obj] = value
  }
}

module.exports = { 
  AxiosSpy,
  MockNuxt 
}
